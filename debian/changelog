python-lesscpy (0.10-2) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Add trailing tilde to min version depend to allow
    backports

 -- Ondřej Nový <novy@ondrej.org>  Sun, 28 Feb 2016 15:41:05 +0100

python-lesscpy (0.10-1) unstable; urgency=medium

  * New upstream release.
  * Updated upstream VCS in debian/copyright and debian/rules.
  * Removed fixup-python3-argparse.patch.
  * Made openstack-pkg-tools optional, and include non-mandatory.
  * Fix shellbang for the python3 package (Closes: #747876).
  * Added extended diff ignore for the egg-info in debian/source/options.

 -- Thomas Goirand <zigo@debian.org>  Thu, 22 May 2014 18:14:47 +0800

python-lesscpy (0.9j-4) unstable; urgency=medium

  * Installing /usr/bin/lesscpy using update-alternatives, so that users can
    use either python 2 or 3.
  * Standard-Version: is now 3.9.5.
  * Replacing the shebang from /usr/bin/pythonx.x to /usr/bin/env python{2,3}.

 -- Thomas Goirand <zigo@debian.org>  Sat, 01 Feb 2014 12:31:15 +0800

python-lesscpy (0.9j-3) unstable; urgency=low

  * d/patches/fixup-python3-argparse.patch: Fix use of argparse for
    compatibility with python3.3.

 -- James Page <james.page@ubuntu.com>  Wed, 02 Oct 2013 14:12:42 +0100

python-lesscpy (0.9j-2) unstable; urgency=low

  * Renamed the MIT license to Expat, after kind explanation from the FTP
    Masters.

 -- Thomas Goirand <zigo@debian.org>  Tue, 01 Oct 2013 01:34:26 +0000

python-lesscpy (0.9j-1) unstable; urgency=low

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 07 Sep 2013 16:48:49 +0800

python-lesscpy (0.9h-1) experimental; urgency=low

  * Initial release. (Closes: #717382)

 -- Thomas Goirand <zigo@debian.org>  Sat, 20 Jul 2013 00:45:19 +0800
